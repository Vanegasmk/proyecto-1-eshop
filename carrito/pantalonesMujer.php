<?php 
include_once '../includes/headerCliente.php';
include_once '../includes/bd.php';
//Muestra pantalones de mujer
$bd = new BaseDeDatos();

$sql = "SELECT * FROM producto WHERE subcategoria = 5 AND genero = 2";

$sentenciaSQL = $bd->Conectar()->prepare($sql);

$sentenciaSQL->execute();

$categorias = $sentenciaSQL->fetchAll(PDO::FETCH_OBJ);

?>



<div class="container">
    <div class="row">
        <?php foreach ($categorias as $categoria) : ?>
            <div class="col-lg-4 col-md-6 mb-4">
                <div class="card h-100">
                    <a href="#"><img class="card-img-top" src="../img/<?= $categoria->imagen ?>"></a>
                    <div class="card-body">
                        <h4 class="card-title">
                            <a href="#"><?= $categoria->nombre ?></a>
                        </h4>
                        <p>Precio</p>
                        <h5>₡<?= $categoria->precio ?></h5>
                        <p><?=$categoria->descripcion ?></p>
                    </div>
                    <div class="card-footer">
                        <center>
                            <a class="btn btn-success btn-icon-split" href="agregarCarrito.php?id=<?=$categoria->id?>">
                                <span class="icon text-white-50">
                                    <i class="fas fa-money-check"></i>
                                </span>
                                <span class="text">Añadir a Carrito</span>

                            </a>
                        </center>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>

</div>






<?php include_once '../includes/footer.php'; ?>