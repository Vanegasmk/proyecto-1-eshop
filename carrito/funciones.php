<?php 

include_once '../includes/productos.php';

function agregarProducto($resultado, $id){//Agregar producto a carro $_SESSION
    
    $_SESSION['carrito'][$id] = array(
        'id' => $resultado['id'],
        'nombre' => $resultado['nombre'],
        'descripcion' => $resultado['descripcion'],
        'imagen' => $resultado['imagen'],
        'precio' => $resultado['precio'],
        'cantidad' => 1
   );
   
}

function TotalPagar(){//Calcular el valor de los productos en el espacio 'cantidad' * precio

    $total = 0;
    if(isset($_SESSION['carrito'])){
        foreach($_SESSION['carrito'] as $indice => $valor){
            $total += $valor['precio'] * $valor['cantidad'];
        }
    }
    return $total;
}


function ActualizarStock(){//Actualizar stock mediante el largo de los elementos
        $producto = new Producto();
        if(isset($_SESSION['carrito'])){
            foreach($_SESSION['carrito'] as $indice => $valor){
                $producto->ActualizarStockProducto($valor['id']);
            }
            unset($_SESSION['carrito']);
        }
        
}

function AgregarOrden($total,$idCliente){//Agrega la orden a la BD
    
    $producto = new Producto();

    $producto->AgregarOrdenFinal($total,$idCliente);
}

function ObtenerRefenciaOrden(){//Obtiene el id de la orden

    $producto = new Producto();

    return $producto->ObtenerIdOrden();
}



function AgregarProductosConRefencia($idOrden,$idCliente){//Agrega productos comprados con el id cliente y id de la orden 
    $producto = new Producto();
        if(isset($_SESSION['carrito'])){
            foreach($_SESSION['carrito'] as $indice => $valor){
                $producto->InsertarProductosComprados($valor['descripcion'],$valor['cantidad'],$valor['precio'],$valor['imagen'],$idOrden,$idCliente);
            }
        }
}


function MostrarDinero($id){//Mostrar dinero gastado mediante id 
    $producto = new Producto();

    return $producto->MostrarDineroGastadoID($id);
    

}

function MostrarTotalComprados($id){//Muestra la cantidad de productos comprados 
    $producto = new Producto();

    return $producto->MostrarCantidadCompradoPorID($id);

}

function MostrarTotalClientesRegistrados(){//Muestra la cantidad de clientes registrados
    $producto = new Producto();

    return $producto->MostrarCantidadDeClientes();

}

function MostrarDineroGanado(){//Dinero ganando por las compras de los clientes
    $producto = new Producto();

    return $producto->MostrarDineroGanado();
}

function MostrarProductosVendidos(){//Muestra la cantidad de productos vendidos a los clientes
    $producto = new Producto();

    return $producto->MostrarCantidadDeProductosVendidos();
}

function MostrarPedidosRealizados($idCliente){//Muestra pedidos realizados
    $producto = new Producto();

    return $producto->MostrarPedidosId($idCliente);
}

function MostrarInfoPedidos($idCliente,$idOrden){//Muestra los productos enlazados a la orden realizada
    $producto = new Producto();

    return $producto->MostrarInfoProductosComprados($idCliente,$idOrden);
}

function MostrarBajoEnStock($idCliente){//Muestra los id de productos bajos en stock
    
    $texto = "";

    $producto = new Producto();

    $productos = $producto->MostrarProductosMedianteCantidad($idCliente);

    foreach($productos as $producto){
        $texto .= "ID Producto: " . $producto->id . "<br>";
    }

    return $texto;
}



?>