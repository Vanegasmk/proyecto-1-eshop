<?php
include_once '../includes/headerCliente.php';
include_once 'funciones.php'
//Muestra las ordenes realizadas del cliente
?>


<div class="container">
    <h1>&nbsp;&nbsp;Pedidos Realizados</h1>


    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table id="tablaPersonas" class="table table-striped table-bordered table-condensed" style="width:100%">
                        <thead class="text-center">
                            <tr>
                                <th>ID</th>
                                <th>Fecha de Compra </th>
                                <th>Total</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <?php
                        $pedidos = MostrarPedidosRealizados($_SESSION['idCliente']);
                        if ($pedidos != null) :
                        ?>
                            <?php foreach ($pedidos as $pedido) : ?>
                                <th><?= $pedido->id ?></th>
                                <th><?= $pedido->fecha ?></th>
                                <th>₡<?= $pedido->total ?></th>
                                <th>
                                    <form class="text-center">

                                        <a class="btn btn-info" href="pedidosRealizadosInfo.php?id=<?=$pedido->id?>">
                                            <i class="fas fa-info"></i>
                                            Ver Info
                                        </a>

                                    </form>
                                </th>
                                </tbody>
                            <?php endforeach; ?>
                        <?php else : ?>
                            <td colspan="6">No hay pedidos realizados</td>
                        <?php endif; ?>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>











<?php include_once '../includes/footer.php'; ?>