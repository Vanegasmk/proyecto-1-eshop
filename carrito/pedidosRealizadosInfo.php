<?php 
include_once '../includes/headerCliente.php';
include_once 'funciones.php';
//Muestra el contenido de las ordenes, los productos
?>


<div class="container">
    <h1>&nbsp;&nbsp;Productos del pedido</h1>


    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table id="tablaPersonas" class="table table-striped table-bordered table-condensed" style="width:100%">
                        <thead class="text-center">
                            <tr>
                                <th>Descripción</th>
                                <th>Cantidad</th>
                                <th>Imagen</th>
                                <th>Precio</th>
                            </tr>
                        </thead>
                        <?php
                        $idOrden = $_GET['id'];
                        $pedidos = MostrarInfoPedidos($_SESSION['idCliente'],$idOrden);
                        if ($pedidos != null) :
                        ?>
                            <?php foreach ($pedidos as $pedido) : ?>
                                <th><center><?= $pedido->descripcion ?></center></th>
                                <th><center><?= $pedido->cantidad?></center></th>
                                <th><center><img src="../img/<?=$pedido->imagen?>" height="70px"></center></th>
                                <th>₡<?= $pedido->precio ?></th>
                                </tbody>
                            <?php endforeach; ?>
                        <?php else : ?>
                            <td colspan="6">No hay productos enlazados en esta órden</td>
                        <?php endif; ?>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>





<?php include_once '../includes/footer.php'; ?>