<?php
include_once '../includes/headerCliente.php';
include_once 'funciones.php';
//Muestra el contenido a comprar en el carrito
?>



<div class="container">
    <h1>&nbsp;&nbsp;Carrito</h1>


    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table id="tablaPersonas" class="table table-striped table-bordered table-condensed" style="width:100%">
                        <thead class="text-center">
                            <tr>
                                <th>Nombre</th>
                                <th>Descripción</th>
                                <th>Imagen</th>
                                <th>Cantidad</th>
                                <th>Total</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <?php if (isset($_SESSION['carrito']) && !empty($_SESSION['carrito'])) : ?>
                            <?php foreach ($_SESSION['carrito'] as $producto => $info) : ?>
                                <tbody>
                                    <th><?php print $info['nombre'] ?></th>
                                    <th><?php print $info['descripcion'] ?></th>
                                    <th><img src="../img/<?php print $info['imagen'] ?>" height="70px"></th>
                                    <th><?php print $info['cantidad'] ?></th>
                                    <th>₡<?php print $info['precio'] ?></th>
                                    <th>
                                        <form class="text-center">

                                            <a onclick="return confirm('¿Desea eliminarlo?')" class="btn btn-danger" href="eliminarProducto.php?id=<?php print $info['id'] ?>">
                                                <i class="fas fa-trash"></i>
                                                Eliminar
                                            </a>

                                        </form>
                                    </th>
                                </tbody>
                            <?php endforeach; ?>
                            <tfoot>
                                <tr>
                                    <td colspan="4" class="text-right">Total</td>
                                    <td>₡<?php print TotalPagar(); ?></td>
                                    <td>

                                        <center>


                                            <a class="btn btn-info" id="botonPagar" href="pagar.php?total= <?php print TotalPagar(); ?>">
                                                <i class="fas fa-money-bill-wave-alt"></i>
                                                Pagar
                                            </a>


                                        </center>
                                    </td>
                                </tr>
                            </tfoot>
                        <?php else : ?>
                            <td colspan="6">No hay elementos en el carrito</td>
                        <?php endif; ?>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>
<br><br><br><br><br><br><br><br><br><br><br><br><br><br>
<?php


?>





<?php
include_once '../includes/footer.php'
?>