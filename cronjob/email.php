<?php
require '../vendor/phpmailer/phpmailer/src/Exception.php';
require '../vendor/phpmailer/phpmailer/src/PHPMailer.php';
require '../vendor/phpmailer/phpmailer/src/SMTP.php';
include_once '../carrito/funciones.php';


use phpmailer\phpmailer\PHPMailer;
use phpmailer\phpmailer\SMTP;
use phpmailer\phpmailer\Exception;

// Load Composer's autoloader
require '../vendor/autoload.php';

// Instantiation and passing `true` enables exceptions
$mail = new PHPMailer(true);

try {
  //Server settings
  $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      // Enable verbose debug output
  $mail->isSMTP();                                            // Send using SMTP
  $mail->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
  $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
  $mail->Username   = 'vanegasmk@gmail.com';                     // SMTP username
  $mail->Password   = 'Sett1530';                               // SMTP password
  $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
  $mail->Port       = 587;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above
  //Valor que va incresar por terminal
  $valor = $argv[1];

  if (is_numeric($valor)) {
    //Recipients
    $mail->setFrom('vanegasmk@gmail.com');
    $mail->addAddress('kvanegasm@est.utn.ac.cr');

    // Content
    $mail->isHTML(true);                                  // Set email format to HTML
    $mail->Subject = 'Elementos Bajos En Stock';
    $mail->Body    =  MostrarBajoEnStock($valor);
    $mail->AltBody = 'Mensaje para el admin correspondiente';

    $mail->send();
    echo 'Message has been sent';
  }else{
    echo "Debe de ingresar datos númericos" .  "\n";
  }
} catch (Exception $e) {
  echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
}
