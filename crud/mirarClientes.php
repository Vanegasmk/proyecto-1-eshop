<?php

include '../includes/header.php';
include_once '../includes/bd.php';

$bd = new BaseDeDatos();

$sql = "SELECT * FROM usuario WHERE rol = 2";

$sentenciaSQL = $bd->Conectar()->prepare($sql);

$sentenciaSQL->execute();

$usuarios = $sentenciaSQL->fetchAll(PDO::FETCH_OBJ);

?>



<div class="container">
    <h1>&nbsp;&nbsp;Clientes Registrados</h1>


    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table id="tablaPersonas" class="table table-striped table-bordered table-condensed" style="width:100%">
                        <thead class="text-center">
                            <tr>
                                <th>Nombre</th>
                                <th>Apellidos</th>
                                <th>Correo</th>
                                <th>Teléfono</th>
                                <th>Dirección</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <?php foreach ($usuarios as $usuario) : ?>
                            <tbody>
                                <th><?= $usuario->nombre; ?></th>
                                <th><?= $usuario->apellido; ?></th>
                                <th><?= $usuario->correo; ?></th>
                                <th><?= $usuario->telefono?></th>
                                <th><?= $usuario->direccion; ?></th>
                                <th>
                                    <form class="text-center">

                                        <a class="btn btn-primary" href="../crud/editarClientes.php?id=<?= $usuario->id ?>">
                                            <i class="fas fa-user-edit"></i>
                                            Editar
                                        </a>

                                        <a onclick="return confirm('¿Desea eliminarlo?')" class="btn btn-danger" href="../crud/eliminarClientes.php?id=<?= $usuario->id ?>">
                                            <i class="fas fa-trash"></i>
                                            Eliminar
                                        </a>

                                    </form>
                                </th>
                            </tbody>
                        <?php endforeach; ?>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>




<?php include_once '../includes/footer.php'; ?>