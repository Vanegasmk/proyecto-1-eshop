<?php

include '../includes/header.php';
include_once '../includes/bd.php';

$bd = new BaseDeDatos();

$sql = "SELECT * FROM categoria";

$sentenciaSQL = $bd->Conectar()->prepare($sql);

$sentenciaSQL->execute();

$categorias = $sentenciaSQL->fetchAll(PDO::FETCH_OBJ);

?>



<div class="container">
    <h1>&nbsp;&nbsp;Categorías Registradas</h1>


    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table id="tablaPersonas" class="table table-striped table-bordered table-condensed" style="width:100%">
                        <thead class="text-center">
                            <tr>
                                <th>Id</th>
                                <th>Nombre</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <?php foreach ($categorias as $categoria) : ?>
                            <tbody>
                                <th><?=$categoria->id ?></th>
                                <th><?= $categoria->nombre; ?></th>
                                <th>
                                    <form class="text-center">

                                        <a class="btn btn-primary" href="../crud/editarCategoria.php?id=<?= $categoria->id ?>">
                                            <i class="fas fa-user-edit"></i>
                                            Editar
                                        </a>

                                        <a onclick="return confirm('¿Desea eliminarlo?')" class="btn btn-danger" href="../crud/eliminarCategoria.php?id=<?= $categoria->id ?>">
                                            <i class="fas fa-trash"></i>
                                            Eliminar
                                        </a>

                                    </form>
                                </th>
                            </tbody>
                        <?php endforeach; ?>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>




<?php include_once '../includes/footer.php'; ?>