<?php

include '../includes/header.php';
include_once '../includes/bd.php';

$bd = new BaseDeDatos();

$sql = "SELECT * FROM producto";

$sentenciaSQL = $bd->Conectar()->prepare($sql);

$sentenciaSQL->execute();

$productos = $sentenciaSQL->fetchAll(PDO::FETCH_OBJ);

?>



<div class="container">
    <h1>&nbsp;&nbsp;Productos Registrados</h1>


    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table id="tablaPersonas" class="table table-striped table-bordered table-condensed" style="width:100%">
                        <thead class="text-center">
                            <tr>
                                <th>Nombre</th>
                                <th>Descripción</th>
                                <th>Género</th>
                                <th>Stock</th>
                                <th>Precio</th>
                                <th>Imagen</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <?php foreach ($productos as $producto) : ?>
                            <tbody>
                                <th><?=$producto->nombre ?></th>
                                <th><?=$producto->descripcion; ?></th>
                                <?php if($producto->genero == 1)  :?>
                                    <th>Hombre</th>
                                <?php else : ?>
                                    <th>Mujer</th>
                                <?php endif; ?>
                                <th><?=$producto->stock; ?></th>
                                <th><?=$producto->precio;?></th>
                                <th><center><img height="70px" src="../img/<?=$producto->imagen;?>"></center></th>
                                <th>
                                    <form class="text-center">

                                        <a class="btn btn-primary" href="../crud/editarProducto.php?id=<?= $producto->id ?>">
                                        <i class="fas fa-user-edit"></i>
                                            Editar
                                        </a>

                                        <a onclick="return confirm('¿Desea eliminarlo?')" class="btn btn-danger" href="../crud/eliminarProducto.php?id=<?= $producto->id ?>">
                                            <i class="fas fa-trash"></i>
                                            Eliminar
                                        </a>

                                    </form>
                                </th>
                            </tbody>
                        <?php endforeach; ?>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>




<?php include_once '../includes/footer.php'; ?>