<?php
include_once '../includes/productos.php';
include_once '../includes/sesion_usuario.php';
include_once '../includes/bd.php';

session_start();

$producto = new Producto();

$sesion_usuario = new SesionUsuario();

$bd = new BaseDeDatos();

$sesion_usuario->RedireccionarAdmin();

$sql = "SELECT * FROM categoria";

$sentenciaSQL = $bd->Conectar()->prepare($sql);

$sentenciaSQL->execute();

$categorias = $sentenciaSQL->fetchAll(PDO::FETCH_OBJ);


if (
    isset($_POST['nombre']) && isset($_POST['descripcion']) && isset($_POST['precio']) && isset($_POST['categoria'])
    && isset($_POST['stock']) && isset($_POST['genero'])
) {

    $nombre = $_POST['nombre'];
    $descripcion = $_POST['descripcion'];
    $precio = $_POST['precio'];
    $categoria = $_POST['categoria'];
    $stock = $_POST['stock'];
    $genero = $_POST['genero'];

    $nombreArchivo = $_FILES['imagen']['name'];

    $rutaTemp = $_FILES['imagen']['tmp_name'];

    $ruta = "../img/" . $nombreArchivo;

    move_uploaded_file($rutaTemp, $ruta);



    $sql = "INSERT INTO producto (nombre,descripcion,imagen,stock,precio,subcategoria,genero) VALUES (:nombre,:descripcion,:imagen,:stock,:precio,:categoria,:genero);";

    $sentencialSQL = $bd->Conectar()->prepare($sql);

    if ($sentencialSQL->execute([
        ':nombre' => $nombre, ':descripcion' => $descripcion, ':imagen' => $nombreArchivo, ':stock' => $stock,
        ':precio' => $precio, ':categoria' => $categoria, ':genero' => $genero
    ])) {
        header('Location:../crud/mirarProducto.php');
    }
}



?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>eShop - Dashboard</title>

    <!-- Custom fonts for this template-->
    <link href="../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">


    <!-- Custom styles for this template-->
    <link href="../css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="../views/admin.php">
                <div class="sidebar-brand-icon rotate-n-15">
                    <i class="fas fa-user-shield"></i>
                </div>
                <div class="sidebar-brand-text mx-3">eShop Admin</div>
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">



            <!-- Divider -->
            <hr class="sidebar-divider">

            <!-- Heading -->
            <div class="sidebar-heading">
                Interface
            </div>

            <!-- Nav Item - Pages Collapse Menu -->
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                    <i class="fas fa-users"></i>
                    <span>Usuarios</span>
                </a>
                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <a class="collapse-item" href="../crud/mirarClientes.php">Mirar</a>
                        <a class="collapse-item" href="../crud/agregarUsuario.php">Agregar</a>
                    </div>
                </div>
            </li>

            <!-- Nav Item - Utilities Collapse Menu -->
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
                    <i class="fas fa-tasks"></i>
                    <span>Categorías</span>
                </a>
                <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <a class="collapse-item" href="mirarCategoria.php">Mirar</a>
                        <a class="collapse-item" href="agregarCategoria.php">Agregar</a>
                    </div>

            </li>


            <!-- Nav Item - Pages Collapse Menu -->
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
                    <i class="fas fa-fw fa-socks"></i>
                    <span>Productos</span>
                </a>
                <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <a class="collapse-item" href="../crud/mirarProducto.php">Mirar</a>
                        <a class="collapse-item" href="../crud/agregarProducto.php">Agregar</a>
                    </div>
                </div>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block">

            <!-- Sidebar Toggler (Sidebar) -->
            <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>


        </ul>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                    <!-- Sidebar Toggle (Topbar) -->
                    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                        <i class="fa fa-bars"></i>
                    </button>



                    <!-- Topbar Navbar -->
                    <ul class="navbar-nav ml-auto">

                        <!-- Nav Item - Search Dropdown (Visible Only XS) -->
                        <li class="nav-item dropdown no-arrow d-sm-none">
                            <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-search fa-fw"></i>
                            </a>
                            <!-- Dropdown - Messages -->
                            <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
                                <form class="form-inline mr-auto w-100 navbar-search">
                                    <div class="input-group">
                                        <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                                        <div class="input-group-append">
                                            <button class="btn btn-primary" type="button">
                                                <i class="fas fa-search fa-sm"></i>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </li>



                        <!-- Nav Item - User Information -->
                        <li class="nav-item dropdown no-arrow">
                            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="mr-2 d-none d-lg-inline text-gray-600 small">Bienvenido <?php echo $_SESSION['nombre']; ?></span>
                            </a>
                            <!-- Dropdown - User Information -->
                            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                                <a class="dropdown-item" href="../includes/cerrar_sesion.php">
                                    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Cerrar Sesión
                                </a>
                            </div>
                        </li>

                    </ul>

                </nav>


                <div class="container">

                    <div class="card o-hidden border-0 shadow-lg my-5">
                        <div class="card-body p-0">
                            <!-- Nested Row within Card Body -->
                            <div class="row">
                                <div class="col-lg-5 d-none d-lg-block bg-register-image"></div>
                                <div class="col-lg-7">
                                    <div class="p-5">
                                        <div class="text-center">
                                            <h1 class="h5 text-gray-900 mb-4">Agregar producto</h1>
                                        </div>
                                        <form class="user" method="POST" enctype="multipart/form-data">
                                            <div class="container">

                                                <div class="card-boy">
                                                    <?php if (!empty($alerta)) : ?>
                                                </div>
                                                <div class="card mb-4 py-3 border-bottom-danger">
                                                    <div class="card-body">
                                                        <?= $alerta; ?>
                                                    </div>
                                                </div>
                                            <?php endif; ?>
                                            <div class="form-group row">
                                                <div class="col-sm-6 mb-3 mb-sm-0">
                                                    <h6>Nombre</h6>
                                                    <input type="text" class="form-control" name="nombre" required>
                                                </div>
                                                <div class="col-sm-6 mb-3 mb-sm-0">
                                                    <h6>Descripción</h6>
                                                    <input type="text" class="form-control " name="descripcion" required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-sm-6 mb-3 mb-sm-0">
                                                    <h6>Precio</h6>
                                                    <input type="number" class="form-control" min="1" name="precio" required>
                                                </div>
                                                <div class="col-sm-6 mb-3 mb-sm-0">
                                                    <h6>Categoría</h6>
                                                    <select name="categoria" class="form-control" required>
                                                        <option value="" selected></option>
                                                        <?php foreach ($categorias as $categoria) : ?>
                                                            <option value="<?= $categoria->id ?>"><?= $categoria->nombre ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="from-group row">
                                                <div class="col-sm-6 mb-3 mb-sm-0">
                                                    <h6>Stock</h6>
                                                    <input type="number" class="form-control" min="1" name="stock" required>
                                                </div>
                                                <div class="col-sm-6 mb-3 mb-sm-0">
                                                    <h6>Imagen</h6>
                                                    <input type="file" name="imagen" required>
                                                </div>
                                                <div class="col-sm-6 mb-3 mb-sm-0">
                                                    <h6>Género</h6>
                                                    <select name="genero" class="form-control" required>
                                                        <option value="" selected></option>
                                                        <option value="1">Hombre</option>
                                                        <option value="2">Mujer</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <hr>
                                            <div>
                                                <button type="submit" class="btn btn-primary btn-user btn-block">Ingresar</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <?php include_once '../includes/footer.php'; ?>