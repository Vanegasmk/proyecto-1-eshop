<?php

include_once '../includes/usuarios.php';

$usuario = new Usuario();

if (
  isset($_POST['nombre']) && isset($_POST['apellidos']) && isset($_POST['telefono']) && isset($_POST['direccion']) && isset($_POST['correo']) &&
  isset($_POST['clave']) && isset($_POST['clavec'])
) {
  $nombre = $_POST['nombre'];
  $apellidos = $_POST['apellidos'];
  $telefono = $_POST['telefono'];
  $direccion = $_POST['direccion'];
  $correo = $_POST['correo'];
  $clave = $_POST['clave'];
  $clavec = $_POST['clavec'];

  if ($clave == $clavec) {

    if ($usuario->ValidarExistenciaUsuario($correo) == true) {
      if ($usuario->AgregarUsuario($clave, $nombre, $apellidos, $correo, $direccion,$telefono,2) == true) {
        header('Location: login.php');
      }
    }else{
      $alerta = "El correo ingresado ya existe en el sistema, inicie sesión!";
    }

    
  } else {
    $alerta = "Las contraseñas no coinciden";
  }
}

?>




<!DOCTYPE html>
<html lang="es">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>eShop - Registrar</title>

  <!-- Custom fonts for this template-->
  <link href="/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="/css/sb-admin-2.min.css" rel="stylesheet">

</head>
<br><br>

<body class="bg-gradient-primary">

  <div class="container">

    <div class="card-boy">
      <?php if (!empty($alerta)) : ?>
    </div>
    <div class="card mb-4 py-3 border-bottom-danger">
      <div class="card-body">
        <?= $alerta; ?>
      </div>
    </div>
  <?php endif; ?>


  <div class="container">

    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
          <div class="col-lg-5 d-none d-lg-block bg-register-image"></div>
          <div class="col-lg-7">
            <div class="p-5">
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">¡Crea tu cuenta!</h1>
              </div>
              <form class="user" method="POST">
                <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
                    <input type="text" class="form-control form-control-user" placeholder="Nombre" name="nombre" required>
                  </div>
                  <div class="col-sm-6">
                    <input type="text" class="form-control form-control-user" placeholder="Apellidos" name="apellidos" required>
                  </div>
                </div>
                <div class="from-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
                    <input type="text" class="form-control form-control-user" placeholder="Teléfono" name="telefono" required>
                  </div>
                  <div class="col-sm-6 mb-3 mb-sm-0">
                    <input type="text" class="form-control form-control-user" placeholder="Dirección" name="direccion" required>
                  </div>
                </div>
                <hr>
                <div class="form-group">
                  <input type="email" class="form-control form-control-user" placeholder="Correo" name="correo" required>
                </div>
                <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
                    <input type="password" class="form-control form-control-user" placeholder="Clave" name="clave" required>
                  </div>
                  <div class="col-sm-6">
                    <input type="password" class="form-control form-control-user" placeholder="Repita la clave" name="clavec" required>
                  </div>
                </div>
                <div>
                  <button type="submit" class="btn btn-primary btn-user btn-block">Registrarse</button>
                </div>
              </form>
              <hr>
              <div class="text-center">
                <a class="small" href="login.php">¿Ya tienes cuenta? ¡Inicia Sesión!</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="/vendor/jquery/jquery.min.js"></script>
  <script src="/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="/js/sb-admin-2.min.js"></script>

</body>

</html>