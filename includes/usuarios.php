<?php
include_once 'bd.php';
include_once 'sesion_usuario.php';

class Usuario extends BaseDeDatos
{

    public function ValidarUsuario($correo, $clave)
    {
        $clavemd5 = md5($clave);

        $sentenciaSQL  = $this->Conectar()->prepare("SELECT * FROM usuario WHERE correo = :correo AND clave = :clave");

        $sentenciaSQL->execute([':correo' => $correo, ':clave' => $clavemd5]);

        $fila = $sentenciaSQL->fetch(PDO::FETCH_NUM);

        if ($sentenciaSQL->rowCount()) {
            if ($fila == true) {
                $rol = $fila[7];

                $nombre = $fila[2];

                $id = $fila[0];

                $_SESSION['rol'] = $rol;

                $_SESSION['nombre'] = $nombre;

                $_SESSION['idCliente'] = $id;



                switch ($_SESSION['rol']) {

                    case 1:

                        header('Location: admin.php');

                        break;

                    case 2;

                        header('Location: cliente.php');

                        break;

                    default:
                }
            }
        } else {
            return false;
        }
    }


    public function AgregarUsuario($clave, $nombre, $apellido, $correo, $direccion, $telefono, $rol)
    {

        $sql = "INSERT INTO usuario (clave,nombre,apellido,correo,direccion,telefono,rol) VALUES (md5(:clave),:nombre,:apellido,:correo,:direccion,:telefono,:rol);";

        $sentenciaSQL = $this->Conectar()->prepare($sql);



        if ($sentenciaSQL->execute([
            ':clave' => $clave, ':nombre' => $nombre, ':apellido' => $apellido,
            ':correo' => $correo, ':direccion' => $direccion, 'telefono' => $telefono, ':rol' => $rol
        ])) {

            return true;
        } else {

            return false;
        }
    }


    public function ValidarExistenciaUsuario($correo)
    {
        $sql = "SELECT * FROM usuario WHERE correo = :correo;";

        $sentenciaSQL = $this->Conectar()->prepare($sql);

        $sentenciaSQL->execute([':correo' => $correo]);

        if ($sentenciaSQL->rowCount() == 0) {
            return true;
        } else {

            return false;
        }
    }

    public function EliminarUsuario($id)
    {

        $sql = 'DELETE FROM usuario WHERE id = :id';

        $sentenciaSQL = $this->Conectar()->prepare($sql);

        if ($sentenciaSQL->execute([':id' => $id])) {
            header('Location:../crud/mirarClientes.php');
        }
    }


    public function EditarUsuario($nombre, $apellidos, $telefono, $direccion, $correo, $rol, $id)
    {

        $sql = "UPDATE usuario SET nombre = :nombre, apellido = :apellido, correo = :correo, direccion = :direccion, telefono = :telefono, rol = :rol  WHERE id = :id;";
        $sentenciaSQL = $this->Conectar()->prepare($sql);

        if ($sentenciaSQL->execute([':nombre' => $nombre, ':apellido' => $apellidos, ':correo' => $correo, ':direccion' => $direccion, ':telefono' => $telefono, ':rol' => $rol, ':id' => $id])) {
            header('Location:../crud/mirarClientes.php');
        }
    }
}
