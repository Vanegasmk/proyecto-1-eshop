<?php
include_once 'bd.php';

class Producto extends BaseDeDatos
{


    public function AgregarCategoria($nombre) // Agregar Categoria
    {
        $sql = "INSERT INTO categoria(nombre) VALUES (:nombre);";


        $sentencialSQL = $this->Conectar()->prepare($sql);

        if ($sentencialSQL->execute([':nombre' => $nombre])) {
            header('Location:../crud/mirarProducto.php');
            return true;
        } else {

            return false;
        }
    }



    public function ValidarExistenciaCategoria($nombre) // Validar Existencia Categoria 
    {
        $sql = "SELECT * FROM categoria WHERE nombre = :nombre";

        $sentencialSQL = $this->Conectar()->prepare($sql);

        $sentencialSQL->execute([':nombre' => $nombre]);

        if ($sentencialSQL->rowCount() == 0) {
            return true;
        } else {

            return false;
        }
    }

    public function EditarCategoria($nombre, $id) //Editar Categoria
    {

        $sql = "UPDATE categoria SET nombre = :nombre  WHERE id = :id;";

        $sentencialSQL = $this->Conectar()->prepare($sql);

        if ($sentencialSQL->execute([':nombre' => $nombre, ':id' => $id])) {
            header('Location:../crud/mirarCategoria.php');
        } else {
            header('Location:../crud/mirarCategoria.php');
        }
    }

    public function EliminarCategoria($id) //Eliminar Categoria
    {
        $sql = "DELETE FROM categoria WHERE id = :id;";

        $sentencialSQL = $this->Conectar()->prepare($sql);

        try {
            $sentencialSQL->execute([':id' => $id]);
            header('Location:../crud/mirarCategoria.php');
        } catch (Throwable $th) {
            header('Location:../crud/mirarCategoria.php');
        }
    }

    public function AgregarSubCategoria($nombre, $descripcion, $categoria) //Agregar SubCategoria
    {
        $sql = "INSERT INTO subcategoria (nombre,descripcion,categoria) VALUES (:nombre,:descripcion,:categoria);";

        $sentencialSQL = $this->Conectar()->prepare($sql);

        if ($sentencialSQL->execute([':nombre' => $nombre, ':descripcion' => $descripcion, ':categoria' => $categoria])) {

            return true;
        } else {

            return false;
        }
    }





    public function EliminarSubCategoria($id) //Eliminar Subcategoria
    {

        $sql = "DELETE FROM subcategoria WHERE id = :id";

        $sentencialSQL = $this->Conectar()->prepare($sql);

        if ($sentencialSQL->execute([':id' => $id])) {

            header('Location:../crud/mirarCategoria.php');
        } else {
            header('Location:../crud/mirarCategoria.php');
        }
    }






    public function EliminarProducto($id) //Eliminiar producto
    {
        $sql = "DELETE FROM producto WHERE id = :id ";

        $sentencialSQL = $this->Conectar()->prepare($sql);

        if ($sentencialSQL->execute([':id' => $id])) {
            header('Location:../crud/mirarProducto.php');
        } else {
            header('Location:../crud/mirarProducto.php');
        }
    }


    public function EditarProducto($nombre, $descripcion, $imagen, $stock, $precio, $categoria, $id) // Editar producto
    {

        $nombreArchivo = $_FILES[$imagen]['name'];

        $rutaTemp = $_FILES[$imagen]['tmp_name'];

        $ruta = "../img/" . $nombreArchivo;

        move_uploaded_file($rutaTemp, $ruta);


        $sql = "UPDATE producto SET  nombre = :nombre, descripcion = :descripcion, 
        imagen = :imagen, stock = :id, precio = :precio, subcategoria = :subcategoria WHERE id = :id;";

        $sentencialSQL = $this->Conectar()->prepare($sql);

        if ($sentencialSQL->execute([
            ':nombre' => $nombre, ':descripcion' =>  $descripcion,
            ':imagen' => $nombreArchivo, ':stock' => $stock, ':precio' => $precio, ":categoria" => $categoria, ':id' => $id
        ])) {

            header('Location:../crud/mirarProducto.php');
        }
    }

    public function MirarProducto($id) //Mirar producto
    {

        $sql = "SELECT * FROM producto WHERE id = :id";

        $sentencialSQL = $this->Conectar()->prepare($sql);

        if ($sentencialSQL->execute([':id' => $id])) {

            return $sentencialSQL->fetch();
        }

        return false;
    }

    public function ActualizarStockProducto($id) //Actualizar stock cuando se hace una compra -1
    {
        $sql = "UPDATE producto SET stock = stock - 1 WHERE  id = :id";

        $sentencialSQL = $this->Conectar()->prepare($sql);

        if ($sentencialSQL->execute([':id' => $id])) {
            return true;
        } else {
            return false;
        }
    }


    public function AgregarOrdenFinal($total, $idCliente) //Agregar orden nueva
    {

        $sql = "INSERT INTO orden(fecha,total,idcliente) VALUES(NOW(),:total,:cliente)";

        $sentencialSQL = $this->Conectar()->prepare($sql);

        if ($sentencialSQL->execute([':total' => $total, ':cliente' => $idCliente])) {
            return true;
        } else {
            return false;
        }
    }


    public function ObtenerIdOrden() //Obtener ID de ultima orden para asociar
    {

        $sql = "SELECT * FROM orden ORDER BY id DESC LIMIT 1";

        $sentencialSQL = $this->Conectar()->prepare($sql);

        if ($sentencialSQL->execute()) {
            return $sentencialSQL->fetch();
        }
    }

    public function InsertarProductosComprados($descrip, $cant, $precio, $img, $orden, $idCliente) //Insertar tablar productos_comprados
    {

        $sql = "INSERT INTO productos_comprados (descripcion,cantidad,precio,imagen,orden,idCliente) VALUES (:descrip,:cant,:precio,:img,:orden,:idcliente)";

        $sentencialSQL = $this->Conectar()->prepare($sql);

        if ($sentencialSQL->execute([':descrip' => $descrip, ':cant' => $cant, ':precio' => $precio, ':img' => $img, ':orden' => $orden, ':idcliente' => $idCliente])) {
            return true;
        } else {
            return false;
        }
    }

    public function MostrarCantidadCompradoPorID($id) //Muestra los contenidos comprados por un cliente
    {
        $sql = "SELECT count(*) FROM productos_comprados WHERE idCliente = :id";

        $sentencialSQL = $this->Conectar()->prepare($sql);

        if ($sentencialSQL->execute([':id' => $id])) {
            return $sentencialSQL->fetch();
        }
    }

    public function MostrarDineroGastadoID($id) //Muestra la suma total gastado por el cliente
    {
        $sql = "SELECT SUM(precio) FROM productos_comprados WHERE idCliente = :id";

        $sentencialSQL = $this->Conectar()->prepare($sql);

        if ($sentencialSQL->execute([':id' => $id])) {
            return $sentencialSQL->fetch();
        }
    }


    public function MostrarCantidadDeClientes() //Cantidad de clientes registrado
    {
        $sql = "SELECT count(*) FROM usuario WHERE rol = 2";

        $sentencialSQL = $this->Conectar()->prepare($sql);

        if ($sentencialSQL->execute()) {
            return $sentencialSQL->fetch();
        }
    }

    public function MostrarCantidadDeProductosVendidos() //Muestra la cantidad de productos vendidos a los clientes por total
    {
        $sql = "SELECT count(*) FROM productos_comprados";

        $sentencialSQL = $this->Conectar()->prepare($sql);

        if ($sentencialSQL->execute()) {
            return $sentencialSQL->fetch();
        }
    }

    public function MostrarDineroGanado() //Muestra la suma total del dinero ganado 
    {
        $sql = "SELECT SUM(precio) FROM productos_comprados";

        $sentencialSQL = $this->Conectar()->prepare($sql);

        if ($sentencialSQL->execute()) {
            return $sentencialSQL->fetch();
        }
    }

    public function MostrarPedidosId($id) //Muestra los contenidos comprados por un cliente
    {
        $sql = "SELECT * FROM orden WHERE idcliente = :id";

        $sentencialSQL = $this->Conectar()->prepare($sql);

        if ($sentencialSQL->execute([':id' => $id])) {
            return $sentencialSQL->fetchAll(PDO::FETCH_OBJ);
        }
    }

    public function MostrarInfoProductosComprados($idCliente, $idOrden) //Muestra los contenidos comprados por un cliente
    {
        $sql = "SELECT * FROM productos_comprados WHERE idCliente = :idCliente AND orden = :idOrden";

        $sentencialSQL = $this->Conectar()->prepare($sql);

        if ($sentencialSQL->execute([':idCliente' => $idCliente, ':idOrden' => $idOrden])) {
            return $sentencialSQL->fetchAll(PDO::FETCH_OBJ);
        }
    }


    public function MostrarProductosMedianteCantidad($cant) //Muestrar los productos con una cantidad de stock determinada por stock
    {
        $sql = "SELECT * FROM producto WHERE stock <= :cant";

        $sentencialSQL = $this->Conectar()->prepare($sql);

        if ($sentencialSQL->execute([':cant' => $cant])) {
            return $sentencialSQL->fetchAll(PDO::FETCH_OBJ);
        }
    }
}
