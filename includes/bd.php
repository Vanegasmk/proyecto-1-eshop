<?php 

class BaseDeDatos{
    private $host;
    private $bd;
    private $usuario;
    private $clave;
    private $charset;

    public function  __construct()
    {
        $this->host = 'localhost'; 
        $this->bd = 'eshop'; 
        $this->usuario = 'vanegasmk'; 
        $this->clave = 'admin'; 
        $this->charset = 'utf8mb4'; 
    }


    public function Conectar()
    {
        try{
            $conexion = "mysql:host=" . $this->host . ";dbname=" . $this->bd . ";charset=" . $this->charset;
            
            $opciones = [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                        PDO::ATTR_EMULATE_PREPARES => false];
             
            $sqlPDO = new PDO($conexion, $this->usuario, $this->clave,  $opciones); 
            return $sqlPDO;
        }catch(PDOException $e){
            print_r("Error de Conexión: " . $e->getMessage());
        }
    }
}

?>